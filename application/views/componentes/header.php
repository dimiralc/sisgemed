<!DOCTYPE html>
<html>
    <head>
        <!-- Header con las librerias necesarias para el correcto funcionamiento de las paginas -->
        <meta charset="UTF-8">
        <title>SISGEMED | <?= $titulo;?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- CSS del inicio de sesion-->
        <link href="<?=$url_base;?>css/iniciarSesion.css" rel="stylesheet" type="text/css" />        
        <!-- bootstrap 3.0.2 -->
        <link href="<?=$url_base;?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />        
        <!-- Ionicons -->
        <link href="<?=$url_base;?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <link href="<?=$url_base;?>css/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- jvectormap -->
        <link href="<?=$url_base;?>css/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
        <!-- fullCalendar -->
        <link href="<?=$url_base;?>css/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?=$url_base;?>css/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap wysihtml5 - text editor -->
        <link href="<?=$url_base;?>css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?=$url_base;?>css/AdminLTE.css" rel="stylesheet" type="text/css" />
        <!-- bootstrap 3.0.2 -->
        <link href="<?=$url_base;?>css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- font Awesome -->
        <link href="<?=$url_base;?>css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="<?=$url_base;?>css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?=$url_base;?>css/AdminLTE.css" rel="stylesheet" type="text/css" />          
    </head>
<body class="skin-blue">
<?php
// Notificar todos los errores excepto E_NOTICE 
// Este es el valor predeterminado establecido en php.ini 
error_reporting(E_ALL ^ E_NOTICE);
?>