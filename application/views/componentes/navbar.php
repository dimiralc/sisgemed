<header class="header">
<a href="components" class="logo">
    <img src="http://localhost/CodeIgniter/img/logotipo.png" width=150px>
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- Messages: style can be found in dropdown.less-->
            <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-envelope"></i>
                    <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Tiene 4 Mensajes</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- start message -->
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="http://localhost/CodeIgniter/img/avatar3.png" class="img-circle" alt="User Image"/>
                                    </div>
                                    <h4>
                                        Sr. Claudio Araya
                                        <small><i class="fa fa-clock-o"></i> 5 min antes</small>
                                    </h4>
                                    <p>Diagnostico Medico realizado. </p>
                                </a>
                            </li><!-- end message -->
                            <li>
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="<?=$url_base;?>img/avatar2.png" class="img-circle" alt="user image"/>
                                    </div>
                                    <h4>
                                        Sra. Estefania Gomez
                                        <small><i class="fa fa-clock-o"></i> 2 horas antes</small>
                                    </h4>
                                    <p>Nuevos estudios sobre E. Coli.</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="<?=$url_base;?>img/avatar.png" class="img-circle" alt="user image"/>
                                    </div>
                                    <h4>
                                        Administrador
                                        <small><i class="fa fa-clock-o"></i> Hoy</small>
                                    </h4>
                                    <p>Debe registrar sus credenciales.</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="<?=$url_base;?>img/avatar2.png" class="img-circle" alt="user image"/>
                                    </div>
                                    <h4>
                                        Administrador
                                        <small><i class="fa fa-clock-o"></i> Ayer</small>
                                    </h4>
                                    <p>Datos verificados satisfactoriamente.</p>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="pull-left">
                                        <img src="<?=$url_base;?>img/avatar.png" class="img-circle" alt="user image"/>
                                    </div>
                                    <h4>
                                        TENS. Gerardo Cabalo
                                        <small><i class="fa fa-clock-o"></i> 3 dias</small>
                                    </h4>
                                    <p>Ayuda respecto a un paciente</p>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">Ver todos los mensajes</a></li>
                </ul>
            </li>
            <!-- Notifications: style can be found in dropdown.less -->
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-warning"></i>
                    <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Tiene 10 Notificaciones</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li>
                                <a href="#">
                                    <i class="ion ion-ios7-people info"></i> Se han agregado 5 pacientes hoy
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-warning danger"></i>Debe registrar sus credenciales 
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="fa fa-users warning"></i> Pacientes en espera
                                </a>
                            </li>

                            <li>
                                <a href="#">
                                    <i class="ion ion-ios7-cart success"></i> Ha Completado su cuota de pacientes
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <i class="ion ion-ios7-person danger"></i> Debe validar sus datos
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="footer"><a href="#">Ver todas</a></li>
                </ul>
            </li>
            <!-- Tasks: style can be found in dropdown.less -->
            <li class="dropdown tasks-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-tasks"></i>
                    <span class="label label-danger">9</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="header">Tiene 9 tareas pendientes</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Validar credenciales
                                        <small class="pull-right">20%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">20% Completado</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Añadir datos a la base de datos
                                        <small class="pull-right">40%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">40% Completado</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Consultar informacion sobre E. Coli
                                        <small class="pull-right">60%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">60% Completado</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                            <li><!-- Task item -->
                                <a href="#">
                                    <h3>
                                        Revisar correo de Administradores
                                        <small class="pull-right">80%</small>
                                    </h3>
                                    <div class="progress xs">
                                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">80% Completado</span>
                                        </div>
                                    </div>
                                </a>
                            </li><!-- end task item -->
                        </ul>
                    </li>
                    <li class="footer">
                        <a href="#">Ver todas las tareas</a>
                    </li>
                </ul>
            </li>
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span>Usuario Registrado <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                        <img src="http://localhost/CodeIgniter/img/avatar2.png" class="img-circle" alt="User Image" />
                        <p>
                            Usuario Registrado - Profesional de la Salud
                            <small>Miembro desde Abril 2014</small>
                        </p>
                    </li>
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="col-xs-4 text-center">
                            <a href="#">Centros</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Áreas</a>
                        </div>
                        <div class="col-xs-4 text-center">
                            <a href="#">Salas</a>
                        </div>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="sesionBloqueada.html" class="btn btn-default btn-flat">Bloquear Sesión</a>
                        </div>
                        <div class="pull-right">
                            <a href="iniciarSesion.html" class="btn btn-default btn-flat">Cerrar Sesión</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
</header>
<div class="wrapper row-offcanvas row-offcanvas-left">