<div class="wrapper row-offcanvas row-offcanvas-left">
    <aside class="left-side sidebar-offcanvas">
    <section class="sidebar">
        <div class="user-panel">
            <div class="image">
                <img src="http://localhost/CodeIgniter/img/avatar3.png" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Hola, Usuario Registrado</p>

                <a href="#"><i class="fa fa-circle text-success"></i> En Línea</a>
            </div>
        </div>
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Buscar Paciente..."/>
                <span class="input-group-btn">
                    <button type='submit' name='seach' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
            </div>
        </form>
        <ul class="sidebar-menu">
            <li class="active">
                <a href="<?=$url_base;?>index">
                    <i class="fa fa-dashboard"></i><span>Actividades</span>
                </a>
            </li>
            <li>
                <a href="<?=$url_base;?>perfil">
                    <i class="fa fa-th"></i><span>Perfil</span></a>    
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i>
                        <span>Pacientes</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarPacientes/registrarPaciente"><i class="fa fa-angle-double-right"></i> Registrar Paciente</a></li>
                        <li><a href="<?=$url_base;?>administrarPacientes/eliminarPaciente"><i class="fa fa-angle-double-right"></i> Administrar Paciente</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-bar-chart-o"></i>
                        <span>Administrar Datos</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarMedicamentos"><i class="fa fa-angle-double-right"></i> Medicamentos</a></li>
                        <li><a href="<?=$url_base;?>administrarAlergia"><i class="fa fa-angle-double-right"></i> Alergias</a></li>
                        <li><a href="<?=$url_base;?>administrarEnfermedad"><i class="fa fa-angle-double-right"></i> Enfermedades</a></li>
                        <li><a href="<?=$url_base;?>administrarTratamientos"><i class="fa fa-angle-double-right"></i> Tratamientos</a></li>
                        <li><a href="<?=$url_base;?>administrarVacunas"><i class="fa fa-angle-double-right"></i> Vacunas</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-laptop"></i>
                        <span>Ficha Clinica </span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarFCE/registrarFCE"><i class="fa fa-angle-double-right"></i>Nueva FCE</a></li>
                        <li><a href="<?=$url_base;?>administrarFCE/eliminarFCE"><i class="fa fa-angle-double-right"></i>Eliminar FCE</a></li>
                        <li><a href="<?=$url_base;?>administrarFCE/actualizarFCE"><i class="fa fa-angle-double-right"></i>Actualizar FCE</a></li>
                    </ul>
                   
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-edit"></i>Orden Médica<span></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarOME/agregarOME"><i class="fa fa-angle-double-right"></i>Regitrar OME</a></li>
                        <li><a href="<?=$url_base;?>administrarOME/eliminarOME"><i class="fa fa-angle-double-right"></i>Eliminar OME</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-table"></i><span>Consent. Informado</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarCIE/agregarCIE"><i class="fa fa-angle-double-right"></i> Registrar CIE</a></li>
                        <li><a href="<?=$url_base;?>administrarCIE/eliminarCIE"><i class="fa fa-angle-double-right"></i> Eliminar CIE</a></li>
                    </ul>
                </li>
                <li>
                    <a href="<?=$url_base;?>calendario">
                        <i class="fa fa-calendar"></i><span>Calendario</span>                                
                    </a>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i> <span>Consulta Medica</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="<?=$url_base;?>administrarCME/agregarCME"><i class="fa fa-angle-double-right"></i> Realizar CME</a></li>
                        <li><a href="<?=$url_base;?>administrarCME/actualizarCME"><i class="fa fa-angle-double-right"></i> Editar CME</a></li>
                        <li><a href="<?=$url_base;?>administrarCME/eliminarCME"><i class="fa fa-angle-double-right"></i> Eliminar CME</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-folder"></i><span>Soporte</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a data-toggle="modal" href="#myModal1"><i class="fa fa-angle-double-right"></i> Contacto</a></li>
                        <li><a data-toggle="modal" href="#myModal"><i class="fa fa-angle-double-right"></i> Acerca de Sisgemed</a></li>
                    </ul>
                </li>
            </ul>
        </section>       
    </aside>
</div>