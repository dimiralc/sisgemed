<aside class="right-side">                
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        Pacientes
                        <small>Eliminar Pacietne</small>
                    </h1>
                    <ol class="breadcrumb">
                        <li><a href="<?=$url_base;?>index"><i class="fa fa-dashboard"></i> Inicio</a></li>
                        <li class="active">Eliminar Paciente</li>
                    </ol>
                </section>
                <section class="content">
                    <div class="input-group input-group-sm">
                        <?= form_open('administrarPacientes/buscarPaciente')?>
                        <?php
                            $attributes = 'class="btn btn-info btn-flat"';                            
                            $buscar = array(
                                    'name'=> 'Rut',
                                    'placeholder'=>'Ingrese el Rut del paciente que desea eliminar, sin puntos ni guion',
                                    'size' => '80',
                                    'class' => 'form-control'
                                );                          
                        ?>
                        <span class="input-group-btn">
                            <?= form_input($buscar)?>  
                            <?= form_submit('btoPaciente','Buscar Rut', $attributes)?>
                        </span>
                    </div>
                    <br>                   
                    <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">Datos del Paciente</h3>
                    </div>
                    <div class="box-body">
                        
                        <?php                                     
                        if($pacientes){
                            foreach ($pacientes -> result() as $paciente){ ?>                                    
                            <div class="row">
                                <p>Rut</p>                                            
                            </div>
                            <div>
                                <input type="text" class="form-control" name="infoRut"  value="<?php echo $paciente->rut;?>">                                                                                        
                            </div>
                            <div class="row">
                                <p>Nombre</p>
                                <div>
                                    <input type="text" class="form-control" value="<?php echo $paciente->nombre;?>">
                                </div>
                            </div>
                            <div class="row">
                                <p>Paterno</p>
                                  <div>
                                    <input type="text" class="form-control" value="<?php echo $paciente->paterno;?>" >                                            
                                </div>
                            </div>
                            <div class="row">
                                <p>Materno</p>
                                <div>
                                    <input type="text" class="form-control" value="<?php echo $paciente->materno;?>">                                            
                                </div>
                            </div>
                            <div class="row">
                                <p>Teléfono</p>
                                 <div >
                                    <input type="text" class="form-control" value="<?php echo $paciente->telefono;?>">                                            
                                </div>
                            </div>                                         
                            <div class="row">
                                <p>Dirección</p>
                                <div >
                                    <input type="text" class="form-control" name="infoRut"  value="<?php echo $paciente->direccion;?>">
                                </div>
                            </div>
                            <div class="row">
                                <p>Estado Civil</p>
                                <div>
                                    <input type="text" class="form-control" value="<?php echo $paciente->ecivil;?>">
                                </div>
                            </div>
                            <div class="row">
                                <p>Género</p>
                                <div>
                                    <input type="text" class="form-control" values="<?php echo $paciente->genero;?>">                                            
                                </div>
                            </div>
                            <div class="row">
                                <p>Nivel de Estudios</p>
                                 <div>
                                    <input type="text" class="form-control" value="<?php echo $paciente->estudios;?>" >                                            
                                </div>
                            </div>
                            <div class="row">
                                <p>Enfermedades Preexistentes</p>
                                 <div >
                                    <input type="text" class="form-control" value="<?php echo $paciente->enfermedades;?>">                                            
                                </div>
                            </div>
                            <br>
                            
                                 <?php }
                                        }?>
                            <?= form_submit('btoPaciente','Eliminar Paciente', $attributes)?>
                            <?= form_submit('btoPaciente','Cancelar', $attributes)?>
                            <?= form_close()?>  
                            </div>
                        </div><!-- /.box-body -->      
                    </div>
                </section>
            </aside>